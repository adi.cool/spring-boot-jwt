package com.springbootjwt.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
public class DemoController {

    @GetMapping("/test")
    public String test(Principal principal) {
        return "Test successfull!!!! for user - " + principal.getName(); // principal gets authenticated user
    }
}
