# Spring Security JWT

This project deals with spring boot authentication using spring security with JWT.
User needs to login with correct credentials to generate the json token and then its used to call
the api's as bearer token

#### cmd to generate rsa private token using openssl
```bash
openssl genrsa -out keypair.pem 2048
```

#### cmd to generate rsa public token using openssl
```bash
openssl rsa -in keypair.pem -pubout -out public.pem
```

#### cmd to format the rsa token to particular standard
```bash
openssl pkcs8 -topk8 -inform PEM -outform PEM -nocrypt -in keypair.pem -out private.pem
```

### Code references
- https://github.com/danvega/jwt
- https://github.com/danvega/jwt-username-password
